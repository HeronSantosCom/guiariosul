﻿# SQL Manager 2007 for MySQL 4.3.4.1
# ---------------------------------------
# Host     : 201.36.165.38
# Port     : 3306
# Database : testemaluco


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `testemaluco`;

CREATE DATABASE `testemaluco`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

USE `testemaluco`;

#
# Structure for the `categorias` table : 
#

CREATE TABLE `categorias` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT '''''',
  `id_categoria` INTEGER(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_categoria` (`id_categoria`),
  CONSTRAINT `fk_categorias` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE

)ENGINE=InnoDB COMMENT='InnoDB free: 38912 kB; InnoDB free: 37888 kB; (`id_categoria' CHECKSUM=0 DELAY_KEY_WRITE=0 PACK_KEYS=0 MIN_ROWS=0 MAX_ROWS=0 ROW_FORMAT=COMPACT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

#
# Structure for the `tipo_status` table : 
#

CREATE TABLE `tipo_status` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT '''''',
  PRIMARY KEY (`id`)

)ENGINE=InnoDB COMMENT='InnoDB free: 38912 kB; InnoDB free: 37888 kB' CHECKSUM=0 DELAY_KEY_WRITE=0 PACK_KEYS=0 MIN_ROWS=0 MAX_ROWS=0 ROW_FORMAT=COMPACT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

#
# Structure for the `tipo_nivel` table : 
#

CREATE TABLE `tipo_nivel` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT '''''',
  PRIMARY KEY (`id`)

)ENGINE=InnoDB COMMENT='InnoDB free: 37888 kB' CHECKSUM=0 DELAY_KEY_WRITE=0 PACK_KEYS=0 MIN_ROWS=0 MAX_ROWS=0 ROW_FORMAT=COMPACT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

#
# Structure for the `usuarios` table : 
#

CREATE TABLE `usuarios` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo',
  `nome` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '''''' COMMENT 'Nome',
  `email` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '''''' COMMENT 'Email',
  `senha` VARCHAR(32) COLLATE utf8_general_ci NOT NULL DEFAULT '''''' COMMENT 'Senha',
  `id_tipo_nivel` INTEGER(11) NOT NULL COMMENT 'Relacionamento - Niveis',
  `id_tipo_status` INTEGER(11) NOT NULL COMMENT 'Situacao Cadastral',
  `acessos` INTEGER(11) DEFAULT '0' COMMENT 'Número de Acessos',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `id_tipo_niveis` (`id_tipo_nivel`),
  KEY `id_tipo_status` (`id_tipo_status`),
  CONSTRAINT `fk_usuarios_tipo_nivel` FOREIGN KEY (`id_tipo_nivel`) REFERENCES `tipo_nivel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_usuarios_tipo_status` FOREIGN KEY (`id_tipo_status`) REFERENCES `tipo_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE

)ENGINE=InnoDB COMMENT='InnoDB free: 37888 kB; (`id_tipo_nivel`) REFER `testemaluco/' CHECKSUM=0 DELAY_KEY_WRITE=0 PACK_KEYS=0 MIN_ROWS=0 MAX_ROWS=0 ROW_FORMAT=COMPACT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

#
# Structure for the `conteudos` table : 
#

CREATE TABLE `conteudos` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `titulo` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '''''',
  `descricao` LONGTEXT NOT NULL,
  `datac` TIMESTAMP DEFAULT '0000-00-00 00:00:00' COMMENT 'Data de cadastro',
  `datau` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Data atualizacao',
  `permalink` VARCHAR(255) COLLATE utf8_general_ci NOT NULL DEFAULT '''''',
  `tags` LONGTEXT,
  `acessos` INTEGER(11) DEFAULT '0',
  `id_tipo_status` INTEGER(11) NOT NULL DEFAULT '0',
  `id_usuarios` INTEGER(11) NOT NULL,
  `id_categorias` INTEGER(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_usuarios` (`id_usuarios`),
  KEY `id_tipo_conteudo` (`id_categorias`),
  KEY `id_tipo_status` (`id_tipo_status`),
  CONSTRAINT `fk_conteudos_categorias` FOREIGN KEY (`id_categorias`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_conteudos_tipo_status` FOREIGN KEY (`id_tipo_status`) REFERENCES `tipo_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_conteudos_usuarios` FOREIGN KEY (`id_usuarios`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE

)ENGINE=InnoDB COMMENT='InnoDB free: 37888 kB; (`id_categorias`) REFER `testemaluco/' CHECKSUM=0 DELAY_KEY_WRITE=0 PACK_KEYS=0 MIN_ROWS=0 MAX_ROWS=0 ROW_FORMAT=COMPACT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

#
# Structure for the `anexos` table : 
#

CREATE TABLE `anexos` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(20) COLLATE utf8_general_ci NOT NULL DEFAULT '''''',
  `arquivo` VARCHAR(150) COLLATE utf8_general_ci NOT NULL DEFAULT '''''',
  `id_conteudos` INTEGER(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_conteudo` (`id_conteudos`),
  CONSTRAINT `fk_anexos_conteudos` FOREIGN KEY (`id_conteudos`) REFERENCES `conteudos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE

)ENGINE=InnoDB COMMENT='InnoDB free: 37888 kB; (`id_conteudos`) REFER `testemaluco/c' CHECKSUM=0 DELAY_KEY_WRITE=0 PACK_KEYS=0 MIN_ROWS=0 MAX_ROWS=0 ROW_FORMAT=COMPACT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

#
# Structure for the `tipo_atributo` table : 
#

CREATE TABLE `tipo_atributo` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT '''''',
  PRIMARY KEY (`id`)

)ENGINE=InnoDB COMMENT='InnoDB free: 38912 kB; InnoDB free: 38912 kB; InnoDB free: 3' CHECKSUM=0 DELAY_KEY_WRITE=0 PACK_KEYS=0 MIN_ROWS=0 MAX_ROWS=0 ROW_FORMAT=COMPACT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

#
# Structure for the `atributos` table : 
#

CREATE TABLE `atributos` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(150) COLLATE utf8_general_ci DEFAULT NULL,
  `id_categorias` INTEGER(11) NOT NULL,
  `id_tipo_atributo` INTEGER(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tipo_conteudo` (`id_categorias`),
  KEY `id_tipo_atributo` (`id_tipo_atributo`),
  CONSTRAINT `fk_atributos_categorias` FOREIGN KEY (`id_categorias`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_atributos_tipo_atributo` FOREIGN KEY (`id_tipo_atributo`) REFERENCES `tipo_atributo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE

)ENGINE=InnoDB COMMENT='InnoDB free: 37888 kB; (`id_categorias`) REFER `testemaluco/' CHECKSUM=0 DELAY_KEY_WRITE=0 PACK_KEYS=0 MIN_ROWS=0 MAX_ROWS=0 ROW_FORMAT=COMPACT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

#
# Structure for the `atributos_conteudos` table : 
#

CREATE TABLE `atributos_conteudos` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT,
  `id_conteudos` INTEGER(11) NOT NULL,
  `id_atributos` INTEGER(11) NOT NULL,
  `valor` BLOB,
  PRIMARY KEY (`id`),
  KEY `id_atributo` (`id_atributos`),
  KEY `fk_atributos_conteudos` (`id_conteudos`),
  CONSTRAINT `fk_atributos_conteudos` FOREIGN KEY (`id_conteudos`) REFERENCES `conteudos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_conteudos_atributos` FOREIGN KEY (`id_atributos`) REFERENCES `tipo_atributo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE

)ENGINE=InnoDB COMMENT='InnoDB free: 37888 kB; (`id_conteudos`) REFER `testemaluco/c' CHECKSUM=0 DELAY_KEY_WRITE=0 PACK_KEYS=0 MIN_ROWS=0 MAX_ROWS=0 ROW_FORMAT=COMPACT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';

#
# Data for the `categorias` table  (LIMIT 0,500)
#

INSERT INTO `categorias` (`id`, `nome`, `id_categoria`) VALUES 
  (1,'Notícias',NULL),
  (2,'Classificados',NULL),
  (3,'Páginas',NULL),
  (4,'Imóveis',2),
  (5,'Veículos',2),
  (6,'Usados',2),
  (7,'Empregos',2),
  (8,'Serviços',2),
  (9,'Turismo',1),
  (10,'Tecnologia',1),
  (11,'Entretenimento',1),
  (12,'Policial',1),
  (13,'Politica',1),
  (14,'Geral',1),
  (15,'Economia',1),
  (16,'Regional',1),
  (17,'Guia',NULL);
COMMIT;

#
# Data for the `tipo_status` table  (LIMIT 0,500)
#

INSERT INTO `tipo_status` (`id`, `nome`) VALUES 
  (-1,'Bloqueado'),
  (1,'Ativo'),
  (2,'Pendente'),
  (3,'Confirmação');
COMMIT;

#
# Data for the `tipo_nivel` table  (LIMIT 0,500)
#

INSERT INTO `tipo_nivel` (`id`, `nome`) VALUES 
  (1,'Administrador'),
  (2,'Editor'),
  (3,'Revendedor'),
  (4,'Cliente'),
  (5,'Usuário');
COMMIT;

#
# Data for the `usuarios` table  (LIMIT 0,500)
#

INSERT INTO `usuarios` (`id`, `nome`, `email`, `senha`, `id_tipo_nivel`, `id_tipo_status`, `acessos`) VALUES 
  (1,'Marcus Coelho','mcoelho@guiariosul','123mudar',1,1,1),
  (2,'Heron','heronrsantos@guiariosul.com.br','123mudar',1,1,3);
COMMIT;

#
# Data for the `tipo_atributo` table  (LIMIT 0,500)
#

INSERT INTO `tipo_atributo` (`id`, `nome`) VALUES 
  (1,'selecao_unica'),
  (2,'senha'),
  (3,'texto'),
  (4,'areatexto'),
  (5,'oculto'),
  (6,'cep'),
  (7,'cpf'),
  (8,'moeda'),
  (9,'cnpj'),
  (10,'data'),
  (11,'hora'),
  (12,'arquivo'),
  (13,'caixa'),
  (14,'selecao_multipla'),
  (15,'selecao_sortida'),
  (16,'selecao_inserida'),
  (17,'estado_cidade'),
  (18,'inteiro'),
  (19,'email'),
  (20,'url'),
  (21,'imagem');
COMMIT;

#
# Data for the `atributos` table  (LIMIT 0,500)
#

INSERT INTO `atributos` (`id`, `nome`, `id_categorias`, `id_tipo_atributo`) VALUES 
  (1,'Cep',4,6),
  (2,'Cor',5,3),
  (3,'Ano/Modelo',5,3),
  (4,'Combustível',5,1);
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;