function ajax(loadUrl, htmlObject, viewLoading, viewError) {
    if ((htmlObject!="") && (loadUrl!="")) {

	//visualiza processando...
	if (!(viewLoading == false)) {
	    $(htmlObject).html(viewLoading);
	}

	//carrega pagina
	$.ajax({
	    url: loadUrl,
	    cache: false,
	    success: function(html){
		$(htmlObject).html(html);
	    },
	    error: function(){
		if (!(viewError == false)) {
		    $(htmlObject).html(viewError);
		}
	    }
	});

    }
}