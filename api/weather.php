<?php

ini_set('display_errors', 1);

//define a biblioteca
$source = 'google';
if(isset($_GET["source"])) {
    $source = $_GET["source"];
}

//define o metodo de medida do tempo
$metric = 'c';
if(isset($_GET["metric"])) {
    $metric = $_GET["metric"];
}

//define a localidade padrão
$location = 'Rio de Janeiro';
if ($source == 'yahoo') {
    $location = 'BRXX0201';
}

//define a localidade
if(isset($_GET["location"])) {
    $location = $_GET["location"];
}

//define a requisição
switch ($source) {
    case 'yahoo':
	$weather_content = file_get_contents("http://xml.weather.yahoo.com/forecastrss?p=" . urlencode($location) . "&u=" . $metric);
	break;
    default:
	$weather_content = file_get_contents("http://www.google.com/ig/api?weather=" . urlencode($location));
	break;
}
$xml = simplexml_load_string(utf8_encode($weather_content));

//define nivel de visualização
switch (isset($_GET['debug'])) {
    case true: //nivel: depuração
	print '<pre>';
	var_dump($xml);
	print '</pre>';
	break;

    default: //nivel: padrao
    //carrega dados por biblioteca
	switch ($source) {
	    case 'yahoo': //biblioteca: yahoo
		$description = (string)$xml->channel->item->description;
		$aux = trim(str_replace(array(" />", "/>"), ">", strtolower(str_replace("\n", "", $description))));
		$aux = explode("<br>", $aux);

		$icon = $aux[0];
		$icon_ini = explode('<img src="', $icon);
		$icon_end = explode('">', $icon_ini[1]);
		$icon = $icon_end[0];

		$temp = $aux[2];
		$temp = explode(', ', $temp);

		$conditional = $temp[0];
		$temp = $temp[1];

		$print[] = 'today';
		$print[] = $icon;
		$print[] = str_replace(array("c", "f", " "), "", $temp);
		$print[] = $conditional;

		print join("|", $print);
		break;

	    default: //biblioteca: padrao (google)
		$print[] = 'today';
		$print[] = "http://www.google.com" . (string)$xml->weather->current_conditions->icon['data'];
		switch ($metric) {
		    case 'f':
			$print[] = (string)$xml->weather->current_conditions->temp_f['data'];
			break;
		    default:
			$print[] = (string)$xml->weather->current_conditions->temp_c['data'];
			break;
		}
		$print[] = strtolower((string)$xml->weather->current_conditions->condition['data']);

		print join("|", $print);
		break;
	}
	break;
}

?>