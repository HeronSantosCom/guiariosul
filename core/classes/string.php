<?php

	/* bloqueador de acesso externo */
$url_check = $_SERVER["PHP_SELF"];
if (eregi("string.php", "$url_check")) {
    header("Location: /index.php");
}

/**
 * @author Heron Santos
 * @copyright 2008
 */

/**
 * @class Manipulação de String
 */
class String {
/**
 * Construtor
 */
    public function __construct() {
    }

    /**
     * Remove campos html de formularios
     *
     * @param string $valor
     * @return string
     */
    public function _UNHTMLENTITIES($valor) {
	$valor = trim($valor);
	$trans_tbl = get_html_translation_table(HTML_ENTITIES);
	$trans_tbl = array_flip($trans_tbl);
	return strtr($valor, $trans_tbl);
    }

    /**
     * Converte os textos do banco de dados para html
     *
     * @param string $valor
     * @return string
     */
    public function db2str($valor) {
	
	if (is_array($valor)) {
	    return $valor;
	}
	
	if (strlen($valor) > 0) {
	    $valor = trim($valor);
	    $valor = stripslashes($valor);
	    $valor = html_entity_decode($valor);
	    return (string)$valor;
	}

	return false;
    }

    /**
     * Converte os textos do html para o banco de dados
     *
     * @param string $valor
     * @return string
     */
    public function str2db($valor) {

	if (is_array($valor)) {
	    return $valor;
	}

	if (strlen($valor) > 0) {
	// remove palavras que contenham sintaxe sql
	//$valor = preg_replace("/(from|alter table|select|insert|delete|update|where|drop table|show tables|#|\*|--|\\\\)/i", "", $valor);
	//limpa espaços vazio
	    $valor = trim($valor);
	    //converte tags html e php
	    $valor = htmlentities($valor);
	    //Adiciona barras invertidas a uma string
	    $valor = addslashes($valor);
	    return (string)$valor;
	}

	return false;
    }

    /**
     *	Extrai variaveis
     *
     * @global object $METHOD
     * @global object $MYSQL
     * @param string $METHOD [GET|POST]
     * @param string $TABLE
     * @param string $ID
     * @return mixed
     */
    public function get_variables($METHOD = 'GET', $TABLE = null, $ID = null) {
	switch ($METHOD) {
	    case 'POST':
	    case 'GET':
		global $$METHOD;
		if (isset($$METHOD)) {
		    foreach ($$METHOD as $field => $value) {
			if (preg_match('#_ddd#', $field)) {
			    $field_aux = str_replace('_ddd', '', $field);
			    $array["$field_aux"] = (string)$$METHOD->$field;
			}
			if (preg_match('#_number#', $field)) {
			    $field_aux = str_replace('_number', '', $field);
			    $array["$field_aux"] = (string)$$METHOD->$field;
			}
			$array["$field"] = (string)$$METHOD->$field;
		    }
		}
	    default:
		global $MYSQL;
		if (strlen($TABLE) > 0) {
		    $qry = $MYSQL->getFields("$TABLE");
		    if (is_array($qry)) {
			foreach ($qry as $field) {
			    if (!isset($$METHOD->$field)) {
				$array["$field"] = null;
			    }
			}
		    }
		}
		break;
	}

	if (isset($array)) {
	    return $array;
	}

	return false;
    }

    /**
     * Metodo que retorna a data formatada
     *
     * @param string $data
     * @param string $formato_atual
     * @param string $formato_novo
     * @return string
     */
    public function toDate($data, $formato_atual, $formato_novo) {
	$formato_atual = strtoupper($formato_atual);
	$formato_novo = strtoupper($formato_novo);
	$dia = substr($data, strpos($formato_atual, "DD"), 2);
	$mes = substr($data, strpos($formato_atual, "MM"), 2);
	$ano = substr($data, strpos($formato_atual, "YYYY"), 4);
	$data_nova = str_replace("DD", $dia, $formato_novo);
	$data_nova = str_replace("MM", $mes, $data_nova);
	$data_nova = str_replace("YYYY", $ano, $data_nova);
	return $data_nova;
    }

    /**
     * Criptografa caracteres na base 64
     *
     * @param string $value
     * @return string
     */
    public function crypTo($value) {
	$value = base64_encode($value);
	$value = str_replace(array('+', '/', '='), array('-', '_', '.'), $value);
	return $value;
    }

    /**
     * Descriptografa caracteres na base 64
     *
     * @param string $value
     * @return string
     */
    public function deCrypt($value) {
	$value = str_replace(array('-', '_', '.'), array('+', '/', '='), $value);
	$mod4 = strlen($value) % 4;
	if ($mod4) {
	    $value .= substr('====', $mod4);
	}
	return base64_decode($value);
    }

    /**
     * Segundos para horas
     *
     * @param int $seconds
     * @return string
     */
    public function second2hour($seconds) {
	if ($seconds > 0) {
	    return @gmdate("H:i:s", $seconds);
	} else {
	    return "00:00:00";
	}
    }

    /**
     * Metodo que retorna a data acrescida
     *
     * @param string $data
     * @param int $dias
     * @param int $meses
     * @param int $ano
     * @return string
     */
    public function sumDate($data, $dias = 0, $meses = 0, $ano = 0) {
	$data = explode("/", $data);
	$newData = date("d/m/Y", mktime(0, 0, 0, $data[1] + $meses, $data[0] + $dias, $data[2] + $ano));
	return $newData;
    }

    /**
     * Metodo que retorna o dia da semana
     *
     * @param string $data
     * @return string
     */
    public function getDayWeek($data) {
	$data = explode("/", $data);
	$dayweek = date("w", mktime(0, 0, 0, $data[1] + $meses, $data[0] + $dias, $data[2] + $ano)) + 1;
	return $dayweek;
    }

    /**
     * Metodo que retorna a string sem acentuação
     *
     * @param string $string
     * @return string
     */
    public function delKeyDead($string, $specials = false) {

	$string = str_replace("Á", "A", $string);
	$string = str_replace("À", "A", $string);
	$string = str_replace("Â", "A", $string);
	$string = str_replace("Ã", "A", $string);
	$string = str_replace("Ä", "A", $string);
	$string = str_replace("á", "a", $string);
	$string = str_replace("à", "a", $string);
	$string = str_replace("â", "a", $string);
	$string = str_replace("ã", "a", $string);
	$string = str_replace("ä", "a", $string);

	$string = str_replace("É", "E", $string);
	$string = str_replace("È", "E", $string);
	$string = str_replace("Ê", "E", $string);
	$string = str_replace("Ë", "E", $string);
	$string = str_replace("é", "e", $string);
	$string = str_replace("è", "e", $string);
	$string = str_replace("ê", "e", $string);
	$string = str_replace("ë", "e", $string);

	$string = str_replace("É", "I", $string);
	$string = str_replace("Ì", "I", $string);
	$string = str_replace("Î", "I", $string);
	$string = str_replace("Ï", "I", $string);
	$string = str_replace("í", "i", $string);
	$string = str_replace("ì", "i", $string);
	$string = str_replace("î", "i", $string);
	$string = str_replace("ï", "i", $string);

	$string = str_replace("Ó", "O", $string);
	$string = str_replace("Ò", "O", $string);
	$string = str_replace("Ô", "O", $string);
	$string = str_replace("Õ", "O", $string);
	$string = str_replace("Ö", "O", $string);
	$string = str_replace("ó", "o", $string);
	$string = str_replace("ò", "o", $string);
	$string = str_replace("ô", "o", $string);
	$string = str_replace("õ", "o", $string);
	$string = str_replace("ö", "o", $string);

	$string = str_replace("Ú", "U", $string);
	$string = str_replace("Ù", "U", $string);
	$string = str_replace("Û", "U", $string);
	$string = str_replace("Ü", "U", $string);
	$string = str_replace("ú", "u", $string);
	$string = str_replace("ù", "u", $string);
	$string = str_replace("û", "u", $string);
	$string = str_replace("ü", "u", $string);

	$string = str_replace("¹", "1", $string);
	$string = str_replace("²", "2", $string);
	$string = str_replace("³", "3", $string);
	$string = str_replace("ª", "a", $string);
	$string = str_replace("Ç", "C", $string);
	$string = str_replace("ç", "c", $string);
	$string = str_replace("Ñ", "N", $string);
	$string = str_replace("ñ", "n", $string);

	if ($specials) {
	    $string = str_replace("(", "_", $string);
	    $string = str_replace(")", "_", $string);
	    $string = str_replace("{", "_", $string);
	    $string = str_replace("}", "_", $string);
	    $string = str_replace("[", "_", $string);
	    $string = str_replace("]", "_", $string);
	    $string = str_replace("/", "_", $string);
	    $string = str_replace("\\", "_", $string);
	    $string = str_replace("$", "_", $string);
	    $string = str_replace("@", "_", $string);
	    $string = str_replace("!", "_", $string);
	    $string = str_replace("&", "_", $string);
	    $string = str_replace("%", "_", $string);
	    $string = str_replace("?", "_", $string);
	    $string = str_replace(";", "_", $string);
	    $string = str_replace(",", "_", $string);
	    $string = str_replace(" ", "_", $string);
	    $string = str_replace("'", "_", $string);
	    $string = str_replace('"', "_", $string);
	}
	return (string)$string;
    }

    /**
     * Metodo que retorna a string em utf-8
     *
     * @param string $string
     * @return string
     */
    public function toUTF8($string) {
	return mb_convert_encoding($string, "UTF-8", mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true));
    }

    /**
     * Metodo que retorna a string em iso
     *
     * @param string $string
     * @return string
     */
    public function toISO1($string) {
	return mb_convert_encoding($string, "ISO-8859-1", mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true));
    }

    /**
     * Metodo que retorna a string em iso
     *
     * @param string $string
     * @return string
     */
    public function toISO15($string) {
	return mb_convert_encoding($string, "ISO-8859-15", mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true));
    }

    /**
     * Metodo de conversão string para base 16
     * @param string $string
     * @return hexadecimal
     */
    public function str2hex($string) {
	for ($i=0; $i < strlen($string); $i++) {
	    $hex[] = dechex(ord($string[$i]));
	}
	if (isset($hex)) {
	    return join("", $hex);
	}
	return false;
    }

    /**
     * Metodo de conversão base 16 para string
     * @param hexadecimal $hex
     * @return string
     */
    public function hex2str($hex) {
	for ($i=0; $i < strlen($hex)-1; $i+=2) {
	    $string[] = chr(hexdec($hex[$i].$hex[$i+1]));
	}
	if (isset($string)) {
	    return join("", $string);
	}
	return false;
    }

    /**
     * Metodo de criação de codigo com base 16
     * @param string $string
     * @return mixed
     */
    public function str2code($string) {
	$hex = 0;
	for ($i=0; $i < strlen($string); $i++) {
	    $hex += $this->str2hex($string);
	}
	if (isset($hex)) {
	    return $hex;
	}
	return false;
    }
}

?>