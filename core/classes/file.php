<?php

//bloqueador de acesso externo
$url_check = $_SERVER["PHP_SELF"];
if (eregi("file.php", "$url_check")) {
    header("Location: /index.php");
}

/**
 * @author Heron Santos
 * @copyright 2008
 */


/**
 * @class Manipulação de Aquivos
 */
class File {
/**
 * Construtor
 */
    public function __construct() {
    }

    /**
     * Verifica se exista a pasta ou senao o aquivo
     *
     * @param string $value
     * @return bool
     */
    public function is_exists($value) {
	$this->value = $value;
	if (!is_dir($this->value)) {
	    if (!file_exists($this->value)) {
		return false;
	    } else {
		return true;
	    }
	} else {
	    return true;
	}
    }

    /**
     * Extrai informações de arquivo
     *
     * @param string $value
     * @param string $request
     * @param bool $check_file
     * @return mixed
     */
    public function extract_info($value, $request = null, $check_file = false) {
	$checked_file = true;

	if ($check_file == true) {
	    if (!$this->is_exists($value)) {
		$checked_file = false;
	    }
	}

	if ($checked_file == true) {
	    $pathfile = $this->pathinfo_utf($value);

	    if (!empty($request)) {
		switch ($request) {
		    case 'dirname':
			return $pathfile['dirname'];
			break;
		    case 'basename':
			return $pathfile['basename'];
			break;
		    case 'extension':
			return $pathfile['extension'];
			break;
		    case 'filename':
			return $pathfile['filename'];
			break;

		    default:
			return false;
			break;
		}
	    }

	    return $pathfile;
	}

	return false;
    }

    /**
     * Retorna informações do arquivo
     *
     * @param string $path
     * @return mixed
     */
    function pathinfo_utf($path) {
	if (strpos($path, '/') !== false) $basename = end(explode('/', $path));
	elseif (strpos($path, '\\') !== false) $basename = end(explode('\\', $path));
	else return false;
	if (empty($basename)) return false;

	$dirname = substr($path, 0, strlen($path) - strlen($basename) - 1);

	if (strpos($basename, '.') !== false) {
	    $extension = end(explode('.', $path));
	    $filename = substr($basename, 0, strlen($basename) - strlen($extension) - 1);
	} else {
	    $extension = '';
	    $filename = $basename;
	}

	return array ( 'dirname' => $dirname, 'basename' => $basename, 'extension' => $extension, 'filename' => $filename);
    }

    public function filesize($path, $check_file = false, $in_shell = false) {
	$checked_file = true;

	if ($check_file == true) {
	    if (!$this->is_exists($path)) {
		$checked_file = false;
	    }
	}

	if ($checked_file == true) {
	    switch ($in_shell) {
		case false:
		    return filesize($path);
		    break;

		default:
		    exec("sudo du -h " . $path, $return);
		    $xCount = count($return);
		    $xCount--;
		    $xTemp = explode("/", $return[$xCount]);
		    return trim($xTemp[0]);
		    break;
	    }
	}

	return false;
    }
    public function open_external_url($url, $method = 'curl') {
	$data = '';
	if(strtolower($method) == 'curl') {
	    $ch = curl_init($url);
	    ob_start();
	    curl_exec($ch);
	    curl_close($ch);
	    $data = ob_get_contents();
	    ob_end_clean();

	} else if(strtolower($method) == 'fopen') {
		ini_set('user_agent', 'my_agent_signature');
		$file = @fopen($url, 'r');
		if($file) {
		    while(!feof($file)) {
			$data = $data . @fgets($file, 4096);
		    }
		    fclose ($file);
		}
	    }
	if(eregi('301 Moved Permanently',$data)) {
	    preg_match("/HREF=\"(.*)\"/si", $data, $r);
	    return open_external_url($r[1], $method);

	} else return $data;

    }
}

?>