<?php

//bloqueador de acesso interno
$url_check = $_SERVER["PHP_SELF"];
if (eregi("smtp.php", "$url_check")) {
    header("Location: /index.php");
}

/**
 * @author Heron Santos
 * @copyright 2009
 * @class Envio de Mensagem via SMTP
 */
class Smtp {
// config de conexao
    var $conn;
    var $host;
    var $localhost;
    var $debug;
    var $xResource = 0;

    //config da autenticacao
    var $auth = false;

    //config da mensagem
    var $charset = "iso-8589-1";
    var $user;
    var $pass;
    var $header;
    var $message;
    var $onconnected = false;


    /**
     * Instanciador do SMTP
     * @param string $host
     * @param boolean $ssl
     * @param integer $port
     * @param integer $timeout
     */
    function Smtp($host, $ssl = false, $port = 25, $timeout = 45) {

	if ($ssl == "1") {
	    $host = 'ssl://'.$host.':'.$port;
	    $port = 25;
	}

	$this->conn = @fsockopen($host, $port, $errno, $errstr, $timeout);
	$this->host = $host;
	if (isset($_SERVER["HTTP_HOST"])) {
	    $this->localhost = $_SERVER["REMOTE_ADDR"];
	}
    }

    /**
     * Monda Cabeçalho da Mensagem
     * @param string $from_name
     * @param string $from_email
     * @param string $to_name
     * @param string $to_email
     * @param string $subject
     * @param string $boundary
     * @return string
     */
    function toHeader($from_name, $from_email, $to_name, $to_email, $subject, $boundary) {
	if (isset($this->header)) {
	    unset($this->header);
	}
	$this->header[] = 'Message-Id: <' . date('YmdHis') . '.' . md5(microtime()) . '.' . strtoupper($from_email) . '>';
	$this->header[] = 'From: "'. $from_name .'" <' . $from_email . '>';
	$this->header[] = 'To: "'. $to_name .'" <' . $to_email . '>';
	$this->header[] = 'Subject: ' . $subject;
	$this->header[] = 'Date: ' . date('D, d M Y H:i:s O');
	$this->header[] = 'X-MSMail-Priority: Low';
	$this->header[] = 'MIME-Version: 1.0';
	$this->header[] = 'Content-type: multipart/mixed; boundary="' . $boundary . '"';
	$this->header[] = '';
	$header = join("\r\n", $this->header);
	return $header;
    }

    /**
     * Envia mensagem
     * @param string $from_name
     * @param string $from_email
     * @param string $to_name
     * @param string $to_email
     * @param string $subject
     * @param string $container
     * @param string $file [path]
     * @return boolean
     */
    function Send($from_name, $from_email, $to_name, $to_email, $subject, $container = null, $file = null) {
	$stop = false;

	if ($this->Open()) {
	    if (!$stop) {
		$is_file_exists = false;
		if (strlen($file) > 0) {
		    if (file_exists($file)) {
			$handle = fopen($file, 'rb');
			$data = fread($handle,filesize($file));
			$data = chunk_split(base64_encode($data));
			$pathfile = pathinfo($file);
			$filename = $pathfile['basename'];
			$filetype = mime_content_type($file);
			$is_file_exists = true;
		    } else {
			$stop = true;
		    }
		}
		$boundary = md5(uniqid(time()));
	    }

	    if (!$stop and !($this->getCode($this->Put('MAIL FROM: <' . $from_email . '>')) == '250')) {
		$stop = true;
	    }

	    if (!$stop and !($this->getCode($this->Put('RCPT TO: <' . $to_email . '>')) == '250')) {
		$stop = true;
	    }

	    if (!$stop) {
		if (isset($this->message)) {
		    unset($this->message);
		}
		$this->getDebug('SendAttachment(): Start Message');
		$this->message[] = 'DATA';
		$this->message[] = $this->toHeader($from_name, $from_email, $to_name, $to_email, $subject, $boundary);

		// Text Version
		$this->message[] = '--' . $boundary;
		$this->message[] = 'Content-Type: text/plain; charset=' . $this->charset;
		$this->message[] = 'Content-Transfer-Encoding: 8bit';
		$this->message[] = '';

		if ($container != null) {
		    $this->message[] = $container;
		}

		$this->message[] = '';

		if ($is_file_exists) {
		    $this->message[] = '--' . $boundary;
		    $this->message[] = 'Content-Type: ' . $filetype . '; name="' . $filename . '"';
		    $this->message[] = 'Content-Transfer-Encoding: base64';
		    $this->message[] = 'Content-Disposition: attachment; filename=\"' .$filename. '"';
		    $this->message[] = '';
		    $this->message[] = $data;
		    $this->message[] = '';
		    $this->message[] = '';
		}

		$this->message[] = ".";
		$message = join("\r\n", $this->message);
		$this->Put($message);
		$this->Get();
		$this->getDebug('SendAttachment(): End Message');
	    }

	    if (!$stop) {
		return true;
	    }

	}

	return false;
    }

    /**
     * Abre Conexão
     * @return boolean
     */
    function Open() {
	if (is_resource($this->conn) and !$this->onconnected) {
	    socket_set_timeout($this->conn, 0, 45000);
	    $this->getDebug('Open(): Resource Created');
	    if ($this->getCode($this->Get()) == '220') {
		$this->getDebug('Open(): Connection Created');
		$stop = true;

		if (($this->auth == "1") and ($this->getCode($this->Put('EHLO '. $this->host)) == '250')) {
		    $stop = false;
		} elseif ($this->getCode($this->Put('HELO '. $this->host)) == '250') {
		    $stop = false;
		}

		if (!$stop and $this->getCode($this->Put('RSET')) == '250') {
		    $stop = false;
		}

		if (!$stop and $this->getCode($this->Put('AUTH LOGIN')) == '334') {
		    $stop = false;
		}

		if (!$stop and $this->getCode($this->Put(base64_encode($this->user))) == '334') {
		    $stop = false;
		}

		if (!$stop and $this->getCode($this->Put(base64_encode($this->pass))) == '235') {
		    $this->onconnected = true;
		}

	    }
	}

	if ($this->onconnected) {
	    return true;
	}

	$this->getDebug('Open('. $this->host .'): Open Error');
	return false;
    }

    /**
     * Fecha a Conexão
     * @return boolean
     */
    function Close() {
	if (is_resource($this->conn)) {
	    if ($this->getCode($this->Put('QUIT')) == '221' and fclose($this->conn)) {
		if ($this->onconnected) {
		    $this->onconnected = false;
		}
		$this->getDebug('Close(): Connection and Resource Destroyed');
		return true;
	    }
	}
	$this->getDebug('Close(): Open Error');
	return false;
    }

    /**
     * Depura o SMTP
     * @param <type> $message
     */
    function getDebug($message = null) {
	debug($message, 2, 'smtp.php');
    }

    /**
     * Recupera o código da resposta do Servidor
     * @param string $value
     * @param string $length
     * @return mixed
     */
    function getCode($value = null, $length = 3) {
	if (strlen($value) > ($length - 1)) {
	    return substr(trim($value), 0, $length);
	}
	return false;
    }

    /**
     *	Enviar informações ao Servidor
     * @param string $string
     * @param string $print
     * @return mixed
     */
    function Put($string, $print = true) {
	$string .= "\r\n";
	$this->xResource++;
	if (is_resource($this->conn)) {
	    if ($return = fputs($this->conn, $string)) {
		$resource = null;
		if ($return) {
		    $resource = '#' . $return . '.' . $this->xResource;
		}
		$this->getDebug('Put('. $resource .'): ' . $string);
		return $this->Get($resource, $print);
	    }
	}
	return false;
    }

    /**
     * Recupera Informações do Servidor
     * @param string $string
     * @param string $print
     * @param string $return
     * @param string $line
     * @return mixed
     */
    function Get($string = null, $print = true, $return = '', $line = '') {
	if (is_resource($this->conn)) {
	    while(strpos($return, "\r\n") == false or substr($line,3,1) != ' ') {
		$line = fgets($this->conn);
		if ($print) {
		    $this->getDebug('Get('. $string .'): ' . $line);
		}
		$return .= $line;
	    }
	    return $return;
	}

	return false;
    }
}
?>
