<?php

	/* bloqueador de acesso externo */
$url_check = $_SERVER["PHP_SELF"];
if (eregi("mysql.php", "$url_check")) {
    header("Location: /index.php");
}

/**
 * @author Heron Santos
 * @copyright 2008
 */

/**
 * @class Conexão com Banco de Dados
 */
class MySQL {
/**
 * Construtor
 */
    public function __construct() {
	$this->debug = false;
    }

    /**
     * Conector
     *
     * @param string $hostname
     * @param int $port
     * @param string $username
     * @param string $password
     * @param string $name
     */
    public function connect($hostname, $port, $username, $password, $name, $debug = false) {
	$this->hostname = trim($hostname);
	$this->port = trim($port);
	$this->username = trim($username);
	$this->password = trim($password);
	$this->name = trim($name);
	$this->debug = $debug;
    }

    /**
     * Depurador do MySQL
     *
     * @param string $message
     */
    public function _DEBUG($message = null) {
	if ($this->debug and strlen($message) > 0) {
	    print "[" . date('M d H:i:s') . "] MYSQL[" . date('B') . "] " . $message . "<br>\n";
	}
    }

    /**
     * Conecta ao banco de dados
     *
     * @return bool
     */
    public function _CONNECT() {
	$this->_DEBUG("Conectando-se ao: " . $this->hostname);
	$this->connection = mysql_connect("$this->hostname:$this->port", $this->username, $this->password);
	if (!$this->connection) {
	    $this->_DEBUG("Erro ao conectar o banco de dados...");
	    return false;
	}
	return $this->connection;
    }

    /**
     * Seleciona o Banco de Dados
     *
     * @return bool
     */
    public function _SELECT_DB($open = true) {
	$this->_DEBUG("Selecionando: " . $this->name);
	if ($open and !mysql_select_db($this->name, $this->connection)) {
	    $this->_DEBUG("Erro ao selecionar uma base de dados...");
	    return false;
	}
	return true;
    }

    /**
     * Abre conexão com banco de dados
     *
     * @return bool
     */
    public function _OPEN($open = true) {
	$this->_DEBUG("Abrindo banco de dados...");
	if ($this->_CONNECT()) {
	    if ($this->_SELECT_DB($open)) {
		$this->_DEBUG("Conectado com sucesso!");
		return true;
	    }
	}
	return false;
    }

    /**
     * Fecha conexão com banco de dados
     *
     * @return bool
     */
    public function _CLOSE() {
	$this->_DEBUG("Fechando conexão com banco de dados...");
	return mysql_close($this->connection);
    }

    /**
     * Executa rotina em sql
     *
     * @param string $query
     * @return mixed
     */
    public function getQuery($query, $open = true) {
	if ($this->_OPEN($open)) {
	//print $query . "<br>\n";
	    $this->_DEBUG("Executando SQL: " . $query);
	    $query_aux = explode(" ", $query);
	    //print $query;
	    if ($result = mysql_query($query) ) {
		if (is_array($query_aux)) {
		    if ($query_aux[0] == "insert") {
			$id_return = mysql_insert_id();
			if ($id_return) {
			    $this->_DEBUG("Operacao bem sucedida, com retorno de ID: " . $id_return);
			    $this->_CLOSE();
			    return $id_return;
			}
		    }
		    $this->_DEBUG("Operacao bem sucedida!");
		    $this->_CLOSE();
		    return $result;
		}
	    }
	}
	$this->_DEBUG("Erro ao executar operação!");
	$this->_CLOSE();
	return false;
    }

    /**
     * Lista campos da tabela
     *
     * @param string $tabela
     * @return array
     */
    public function getFields($tabela) {
	if ($this->_OPEN()) {
	    $this->_DEBUG("Listando campos da tabela: " . $tabela);
	    if ($result = mysql_list_fields($this->name, $tabela)) {
		if (($fields = mysql_num_fields($result)) > 0) {
		    for ($iCount = 0; $iCount <= $fields - 1; $iCount++) {
			$tmp_return = mysql_field_name($result, $iCount);
			$return[] = $tmp_return;
		    }
		    $this->_DEBUG("Operacao bem sucedida!");
		    $this->_CLOSE();
		    return $return;
		}
	    }
	}
	$this->_DEBUG("Erro ao executar operação!");
	$this->_CLOSE();
	return false;
    }

    /**
     * Lista tabelas do banco de dados
     *
     * @return array
     */
    public function getTables() {
	if ($this->_OPEN()) {
	    $this->_DEBUG("Listando tabelas do banco de dados");
	    if ($result = mysql_list_tables($this->name)) {
		while ($table = mysql_fetch_row($result)) {
		    $return[] = $table[0];
		}
		if (isset($return)) {
		    $this->_DEBUG("Operacao bem sucedida!");
		    $this->_CLOSE();
		    return $return;
		}
	    }
	}
	$this->_DEBUG("Erro ao executar operação!");
	$this->_CLOSE();
	return false;
    }

    /**
     * Retorna um valor da tabela
     *
     * @param string $tabela
     * @param string $coluna
     * @param string $condicao
     * @param string $ordenacao
     * @param string $outros
     * @return mixed
     */
    public function getValue($tabela, $coluna, $condicao = null, $ordenacao = null, $outros = null) {
	if ($fifo = $this->querySelect($tabela, '`'.$coluna.'`', $condicao, $ordenacao, $outros)) {
	    if (is_array($fifo)) {
		foreach ($fifo as $result) {
		    $value = $result["$coluna"];
		    return $value;
		}
	    }
	    return $fifo;
	}
	return false;
    }

    /** Retorna nulo
     *
     * @param string $tabela
     * @return null
     */
    public function dbTruncate($tabela) {

	$sql = "truncate " . $tabela;

	if ($fifo = $this->getQuery($sql)) {
	    return true;
	}

	return false;
    }

    /**
     * Optimiza tabela do banco de dados
     *
     * @param string $tabela
     * @return bool
     */
    public function dbOptimize($tabela) {

	$sql = "optimize table " . $tabela;

	if ($fifo = $this->getQuery($sql)) {
	    return true;
	}

	return false;
    }

    /**
     * Cria Base de Dados
     *
     * @return bool
     */
    public function dbCreate($database = null, $check = false) {
	if (strlen($database) <= 0) {
	    $database = $this->name;
	}

	$sql = "create database ";

	if ($check) {
	    $sql .= ' IF NOT EXISTS ';
	}

	$sql .= $database;

	if ($fifo = $this->getQuery($sql, false)) {
	    return true;
	}

	return false;
    }

    /**
     * Exclui base de dados
     *
     * @return bool
     */
    public function dbDrop($database = null, $check = false) {
	if (strlen($database) <= 0) {
	    $database = $this->name;
	}

	$sql = "drop database ";

	if ($check) {
	    $sql .= ' IF EXISTS ';
	}

	$sql .= $database;

	if ($fifo = $this->getQuery($sql, false)) {
	    return true;
	}

	return false;
    }

    /**
     * Retorna o total de registros
     *
     * @param string $tabela
     * @param string $condicao
     * @return int
     */
    public function dbCount($tabela, $condicao = null) {

	if (strlen($condicao) > 0) {
	    $condicao = " where " . $condicao;
	}

	$sql = "select count(*) as total from " . $tabela . $condicao;

	if ($fifo = $this->getQuery($sql)) {
	    $value = mysql_result($fifo, 0, "total");
	    if (strlen($value) > 0) {
		return $value;
	    }
	}

	return false;
    }

    /**
     * Verifica se existe registro na tabela
     *
     * @param string $tabela
     * @param string $condicao
     * @return bool
     */
    public function dbCheck($tabela, $condicao = null) {

	if (strlen($condicao) > 0) {
	    $condicao = " where " . $condicao;
	}

	$sql = "select count(*) as total from " . $tabela . $condicao;

	if ($fifo = $this->getQuery($sql)) {
	    $value = mysql_result($fifo, 0, "total");
	    if ($value == 0) {
		return false;
	    } else {
		return true;
	    }
	}

	return false;
    }

    /**
     * Retorna o total acumulativo
     *
     * @param string $tabela
     * @param string $coluna
     * @param string $condicao
     * @return string
     */
    public function dbSum($tabela, $coluna, $condicao = null) {

	if (strlen($condicao) > 0) {
	    $condicao = " where " . $condicao;
	}

	$sql = "select sum(" . $coluna . ") as total from " . $tabela . $condicao;

	if ($fifo = $this->getQuery($sql)) {
	    $value = mysql_result($fifo, 0, "total");
	    if (strlen($value) > 0) {
		return $value;
	    }
	}

	return false;
    }

    /** Retorna o total acumulativo
     *
     * @param string $tabela
     * @param string $coluna
     * @param string $condicao
     * @return string
     */
    public function dbMax($tabela, $coluna, $condicao = null) {

	if (strlen($condicao) > 0) {
	    $condicao = " where " . $condicao;
	}

	$sql = "select max(" . $coluna . ") as total from " . $tabela . $condicao;

	if ($fifo = $this->getQuery($sql)) {
	    $value = mysql_result($fifo, 0, "total");
	    if (strlen($value) > 0) {
		return $value;
	    }
	}

	return false;
    }

    /**
     * Retorna a média de um valor
     *
     * @param string $tabela
     * @param string $coluna
     * @param string $condicao
     * @return string
     */
    public function dbAvg($tabela, $coluna, $condicao = null) {

	if (strlen($condicao) > 0) {
	    $condicao = " where " . $condicao;
	}

	$sql = "select avg(" . $coluna . ") as total from " . $tabela . $condicao;

	if ($fifo = $this->getQuery($sql)) {
	    $value = mysql_result($fifo, 0, "total");
	    if (strlen($value) > 0) {
		return $value;
	    }
	}

	return false;
    }

    /**
     * Seleciona dados na tabela
     *
     * @param string $tabela
     * @param string $colunas
     * @param string $condicao
     * @param string $ordenacao
     * @param string $outros
     * @return array
     */
    public function querySelect($tabela, $colunas = " * ", $condicao = null, $ordenacao = null, $outros = null) {

	if (empty($colunas)) {
	    $colunas = " * ";
	}

	if (strlen($condicao) > 0) {
	    $condicao = " where " . $condicao;
	}

	if (strlen($ordenacao) > 0) {
	    $ordenacao = " order by " . $ordenacao;
	}

	if (strlen($outros) > 0) {
	    $outros = " " . $outros;
	}

	$sql = "select " . $colunas . " from " . $tabela . $condicao . $ordenacao . $outros;

	if ($fifo = $this->getQuery($sql)) {
	    while ($result = mysql_fetch_array($fifo, MYSQL_ASSOC)) {
		$array[] = $result;
	    }
	    mysql_free_result($fifo);
	    if (isset($array)) {
		return $array;
	    }
	}

	return false;
    }

    /**
     * Insere dados na tabela
     *
     * @param string $tabela
     * @param string $valores
     * @param string $outros
     * @return mixed
     */
    public function queryInsert($tabela, $valores, $outros = null) {

	if (strlen($outros) > 0) {
	    $outros = " " . $outros;
	}

	$sql = "insert into " . $tabela . " set " . $valores . $outros;

	if ($fifo = $this->getQuery($sql)) {
	    return $fifo;
	}

	return false;
    }

    /**
     * Atualiza dados na tabela
     *
     * @param string $tabela
     * @param string $valores
     * @param string $condicao
     * @param string $outros
     * @return bool
     */
    public function queryUpdate($tabela, $valores, $condicao = null, $outros = null) {

	if (strlen($condicao) > 0) {
	    $condicao = " where " . $condicao;
	}

	if (strlen($outros) > 0) {
	    $outros = " " . $outros;
	}

	$sql = "update " . $tabela . " set " . $valores . $condicao . $outros;

	if ($fifo = $this->getQuery($sql)) {
	    return $fifo;
	}

	return false;
    }

    /**
     * Deleta dados na tabela
     *
     * @param string $tabela
     * @param string $condicao
     * @param string $outros
     * @return bool
     */
    public function queryDelete($tabela, $condicao = null, $outros = null) {

	if (strlen($condicao) > 0) {
	    $condicao = " where " . $condicao;
	}

	if (strlen($outros) > 0) {
	    $outros = " " . $outros;
	}

	$sql = "delete from " . $tabela . $condicao . $outros;

	if ($this->getQuery($sql)) {
	    if ($fifo = $this->dbOptimize($tabela)) {
		return $fifo;
	    }
	}

	return false;
    }
}

?>