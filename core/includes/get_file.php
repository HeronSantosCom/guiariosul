<?php

// bloqueador de acesso externo
$url_check = $_SERVER["PHP_SELF"];
if (eregi("get_file.php", "$url_check")) {
    header("Location: /index.php");
}

/**
 * Funcao de Carregar Include
 * @var path is (file, full)
 */
function getInclude($file, $path = null) {
    return $path . "/core/includes/" . $file;
}

/**
 * Funcao de Carregar Classe
 * @var path is (file, full)
 */
function getClasses($file, $path = null) {
    return $path . "/core/classes/" . $file;
}

/**
 * Funcao de Carregar Library
 * @var path is (file, full)
 */
function getLibrary($file, $path = null) {
    return $path . "/core/library/" . $file;
}

?>