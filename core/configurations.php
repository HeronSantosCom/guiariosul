<?php

function getPath($location, $path = null) {
    if (strlen($path) == 0) {
	$path = pathinfo(__file__);
	$path = $path['dirname'];
    }
    return substr($path, 0, strpos($path, '/' . $location));
}

//determina PATH
$PATH = getPath('core');

//bloqueia acesso direto
$url_check = $_SERVER["PHP_SELF"];
if (eregi("configurations.php", "$url_check")) {
    header("Location: /index.php");
}

//define depuração...
$debug = false;
if (isset($_GET['debug'])) {
    $debug = true;
}

//carrega função de montar filename
include ($PATH . "/core/includes/get_file.php");

//carrega classe de manipulação de string
include (getClasses("string.php", $PATH));

//carrega classe de manipulação de arquivo
include (getClasses("file.php", $PATH));

//carrega classe de manipulação de mysql
include (getClasses("mysql.php", $PATH));

//instancia classe nula
class NullObject {
}

//instancia classe string
$STRING = new String();

//instancia classe file
$FILE = new File();

//instância a classe mysql
$MYSQL = new MySQL();

//inicia conexão mysql
$MYSQL->connect("localhost", "3306", "", "", "guiariosul", $debug);

//define variaveis globais
$GLOBAL = new NullObject();
$qry_globals = $MYSQL->querySelect("`global`");
if (is_array($qry_globals)) {
    foreach ($qry_globals as $tbl_globals) {
	$key = $tbl_globals["var"];
	$val = $tbl_globals["value"];
	$GLOBAL->$key = '';
	if (strlen($val) > 0) {
	    $GLOBAL->$key = $STRING->str2db($val);
	}
    }
}

//session_start();
//header("Content-Type: text/html; charset=utf-8", true);

?>