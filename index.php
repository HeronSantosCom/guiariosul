<?php

    require 'core/configurations.php';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Guia Rio Sul</title>
	<link href="css/template.css" rel="stylesheet" type="text/css" media="screen" />
	<script type="text/javascript" src="js/jquery-1.3.1.js"></script>
	<script type="text/javascript" src="js/jquery.corner.js"></script>
	<script type="text/javascript" src="js/ajax.js"></script>
	<script type="text/javascript" src="js/remove_dead_key.js"></script>
	<script src="http://www.google.com/jsapi?key=ABQIAAAAVM4IIYyVrekp8f7HYaO3TRQdf6l9FA9NJc96-QM1JLstnob6ihRwrRSUdz6TPcMidUg3iI7HA6j3Sw" type="text/javascript"></script>
	<script language="Javascript" type="text/javascript">
	    var country_location = "br";
	    var city_location = "Rio de Janeiro";
	    var region_location = "RJ";
	    //<![CDATA[
	    google.load("search", "1");
	    if (google.loader.ClientLocation != null) {
		country_location = google.loader.ClientLocation.address.country_code.toLowerCase();
		city_location = google.loader.ClientLocation.address.city;
		region_location = google.loader.ClientLocation.address.region;
	    }
	    //]]>
	    var address_location = city_location + ", " + region_location;
	</script>
    </head>
    <body>
	<div id="container">
	    <div id="header">
		<div id="header_logo">
		    <a href="index.php">
			<h1>Guia Rio Sul</h1>
		    </a>
		</div>
		<!-- //TODO: Alinhar para direita... -->
		<div id="header_info">Você está em <img src="imagens/icones/bandeiras/br.gif" align="absmiddle" id="country-flag" /> <a href="#" title="Mudar localidade..."><script>document.write(address_location);</script></a></div>
		<div id="header_account">
		    <ul>
			<li><a href="#">Acessar</a></li>
			<li><a href="#">Esqueci minha senha</a></li>
			<li><a href="#">Sugerir feed</a></li>
			<li><a href="#">Cadastre-se</a></li>
		    </ul>
		</div>
	    </div>
	    <div id="first-menu">
		<ul>
		    <li ><a href="#" class="marcado" >Página inicial de Anônimo</a></li>
		    <li><a href="#">Ao vivo</a></li>
		    <li><a href="#">Meu Histórico</a></li>
		    <li><a href="#">Meus Alertas</a></li>
		    <li><a href="#">Nova</a></li>
		</ul>
	    </div>
	    <div id="second-menu">
		<ul>
		    <li><a href="#" >tecnologia</a></li>
		    <li><a href="#">economia</a></li>
		    <li><a href="#">esportes</a></li>
		    <li><a href="#">rumores</a></li>
		    <li><a href="#">brasil</a></li>
		    <li><a href="#">mangaratiba</a></li>
		    <li><a href="#">politica</a></li>
		    <li><a href="#">divertimento</a></li>
		    <li><a href="#">saúde</a></li>
		    <li class="menu_personalizar"><a href="#" >Personalizar esta página</a></li>
		</ul>
	    </div>

	    <!-- inicio conteudo -->
	    <div id="content"></div>
	    <!-- fim conteudo -->

	    <div id="footer">
		<ul>
		    <li><a href="#" >tecnologia</a></li>
		    <li><a href="#">economia</a></li>
		    <li><a href="#">esportes</a></li>
		    <li><a href="#">rumores</a></li>
		    <li><a href="#">brasil</a></li>
		    <li><a href="#">mangaratiba</a></li>
		    <li><a href="#">politica</a></li>
		    <li><a href="#">divertimento</a></li>
		    <li><a href="#">saúde</a></li>
		    <li><a href="#" >Personalizar esta página</a></li>
		</ul>
		<ul>
		    <li><a href="#" >tecnologia</a></li>
		    <li><a href="#">economia</a></li>
		    <li><a href="#">esportes</a></li>
		    <li><a href="#">rumores</a></li>
		    <li><a href="#">brasil</a></li>
		    <li><a href="#">mangaratiba</a></li>
		    <li><a href="#">politica</a></li>
		    <li><a href="#">divertimento</a></li>
		    <li><a href="#">saúde</a></li>
		    <li><a href="#" >Personalizar esta página</a></li>
		</ul>
		<ul>
		    <li><a href="#" >tecnologia</a></li>
		    <li><a href="#">economia</a></li>
		    <li><a href="#">esportes</a></li>
		    <li><a href="#">rumores</a></li>
		    <li><a href="#">brasil</a></li>
		    <li><a href="#">mangaratiba</a></li>
		    <li><a href="#">politica</a></li>
		    <li><a href="#">divertimento</a></li>
		    <li><a href="#">saúde</a></li>
		    <li><a href="#" >Personalizar esta página</a></li>
		</ul>
		<br clear="all" />
	    </div>
	</div>
    </body>

    <script language="Javascript" type="text/javascript">
	$("#header_info").corner("bl");
	$("#header_account").corner("bl cc:#B79C80");
	$("#country-flag").attr("src","imagens/icones/bandeiras/" + country_location + ".gif");
	$.ajax({
	    url: "api/weather.php?location=" + remove_dead_key(city_location),
	    cache: false,
	    success: function(html){
		$("#content").html(html);
	    },
	    error: function(){
		$("#content").html("viewError");
	    }
	});
    </script>
</html>
